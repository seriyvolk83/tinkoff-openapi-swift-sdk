Pod::Spec.new do |s|
  s.name        = "TinkoffSdk"
  s.version     = "0.0.1"
  s.summary     = "OpenAPI SDK"
  s.homepage    = "https://gitlab.com/seriyvolk83/TinkoffSdk"
  s.license     = { :type => "MIT" }
  s.authors     = { "seriyvolk83" => "volk@frgroup.ru" }

  s.requires_arc = true
  s.swift_version = "5.2"
  s.ios.deployment_target = "10.0"
  s.source   = { :git => "https://gitlab.com/seriyvolk83/tinkoff-openapi-swift-sdk.git", :tag => s.version }
  s.source_files = "Source/*.swift"
  
  s.dependency 'RxAlamofire', '~> 5.0.0'
  s.dependency 'SwiftyJSON', '~> 4.0'
  s.dependency 'SwiftEx83/Int'

end
