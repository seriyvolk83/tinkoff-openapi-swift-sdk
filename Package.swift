// swift-tools-version:5.2

import PackageDescription

let package = Package(
    name: "OpenApi",
    platforms: [
        .macOS(.v10_15),
        .iOS(.v12)
    ],
    products: [
        .library(name: "OpenApi", targets: ["OpenApi"]),
    ],
    dependencies: [
	.package(url: "https://gitlab.com/seriyvolk83/SwiftEx.git", from: "0.4.12"),
    ],
    targets: [
        .target(name: "OpenApi", dependencies: [
                        .product(name: "SwiftExInt", package: "SwiftEx")
                ], path: "Source")
])

