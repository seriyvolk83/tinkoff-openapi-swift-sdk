//
//  OpenApiConfig.swift
//  TinkoffSample
//
//  Created by Volkov Alexander on 6/11/21.
//

import Foundation

public struct OpenApiConfig {
    
    public let apiURL: String
    public let socketURL: String
    public let secretToken: String
    public let brokerAccountId: String?
    
    public init(apiURL: String, socketURL: String, secretToken: String, brokerAccountId: String?) {
        self.apiURL = apiURL
        self.socketURL = socketURL
        self.secretToken = secretToken
        self.brokerAccountId = brokerAccountId
    }
}
