//
//  Enums.swift
//  TinkoffSample
//
//  Created by Volkov Alexander on 6/11/21.
//

import Foundation

public typealias SandboxCurrency = Currency

public enum Currency: String, Codable {
    case RUB = "RUB", USD = "USD", EUR = "EUR", GBP = "GBP", HKD = "HKD", CHF = "CHF", JPY = "JPY", CNY = "CNY", TRY = "TRY"
}

public enum InstrumentType: String, Codable {
    case Stock = "Stock", Currency = "Currency", Bond = "Bond", Etf = "Etf"
}

public enum BrokerAccountType: String, Codable {
    case tinkoff = "Tinkoff", tinkoffIis = "TinkoffIis"
}

public enum TradeStatus: String, Codable {
    case NormalTrading = "NormalTrading", NotAvailableForTrading = "NotAvailableForTrading"
}

public enum OperationType: String, Codable {
    case Buy = "Buy", Sell = "Sell"
}

public enum OperationTypeWithCommission: String, Codable {
    case Buy = "Buy", BuyCard = "BuyCard", Sell = "Sell", BrokerCommission = "BrokerCommission", ExchangeCommission = "ExchangeCommission", ServiceCommission = "ServiceCommission", MarginCommission = "MarginCommission", OtherCommission = "OtherCommission", PayIn = "PayIn", PayOut = "PayOut", Tax = "Tax", TaxLucre = "TaxLucre", TaxDividend = "TaxDividend", TaxCoupon = "TaxCoupon", TaxBack = "TaxBack", Repayment = "Repayment", PartRepayment = "PartRepayment", Coupon = "Coupon", Dividend = "Dividend", SecurityIn = "SecurityIn", SecurityOut = "SecurityOut"
}

public enum OperationStatus: String, Codable {
    case Done = "Done", Decline = "Decline", Progress = "Progress"
}

public enum CandleResolution: String, Codable {
    case m1 = "1min", m2 = "2min", m3 = "3min", m5 = "5min", m10 = "10min", m15 = "15min", m30 = "30min", h = "hour", d = "day", w =  "week", m = "month"
}

public enum OrderStatus: String, Codable {
    case New = "New", PartiallyFill = "PartiallyFill", Fill = "Fill", Cancelled = "Cancelled", Replaced = "Replaced", PendingCancel = "PendingCancel", Rejected = "Rejected", PendingReplace = "PendingReplace", PendingNew = "PendingNew"
}

public enum OrderType: String, Codable {
    case Limit = "Limit", Market = "Market"
}
