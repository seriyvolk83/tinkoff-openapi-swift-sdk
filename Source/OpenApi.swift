//
//  OpenApi.swift
//  TinkoffSample
//
//  Created by Volkov Alexander on 6/11/21.
//

import Foundation
#if canImport(SwiftEx83)
import SwiftEx83
#endif
#if canImport(SwiftExInt)
import SwiftExInt
#endif
import RxSwift
import SwiftyJSON

import Alamofire
import RxAlamofire

/// Rest API alias
typealias API = RestServiceApi

/// API
open class OpenApi {
    
    /// flag: Protects from buying: false - all orders API will crash the app if called; it prevents from buying if you acidentially leaved API calls in debug version of the app; true - no limits.
    public var allowOrders: Bool = true
    
    private let apiURL: String
    private let secretToken: String
    private var brokerAccountId: String?
    
    private var sandboxCreated: Bool = false
    
    public init(config: OpenApiConfig) {
        self.apiURL = config.apiURL
        self.secretToken = config.secretToken
        self.brokerAccountId = config.brokerAccountId
        RestServiceApi.headers = [
            "Authorization": "Bearer \(config.secretToken)",
        ]
    }
    
    // MARK: - Sandbox
    
    public func sandboxClear() -> Observable<Void> {
        self.sandboxRegister().flatMap { _ in
            API.requestVoid(.post,
                            url: self.url("/sandbox/clear" + self.defaultQuery()),
                        parameters: [:])
        }
    }
    
    func sandboxRegister(brokerAccountId: String? = nil) -> Observable<Void> {
        if let brokerAccountId = brokerAccountId {
            self.brokerAccountId = brokerAccountId
            return Observable.just(())
        }
        guard !self.sandboxCreated else { return Observable.just(()) }
        return API.post(url: url("/sandbox/register"), parameters: [:]).map { (user: Payload<UserAccount>) -> Void in
            self.sandboxCreated = true
            self.brokerAccountId = user.payload.brokerAccountId
            return ()
        }
        
    }
    
    // MARK: - Stocks
    
    /// Метод для задания баланса по бумагам
    /// - Parameter params: см. описание типа
    public func setPositionBalance(params: SandboxSetPositionBalanceRequest) -> Observable<Void> {
        return API.requestVoid(.post,
                           url: url("/sandbox/positions/balance" + self.defaultQuery()),
                           parameters: params.dictionary())
    }
    
    /// Метод для задания баланса по валютам
    public func setCurrenciesBalance(params: SandboxSetCurrencyBalanceRequest) -> Observable<Void> {
        return API.requestVoid(.post,
                               url: url("/sandbox/currencies/balance" + self.defaultQuery()),
                               parameters: params.dictionary())
    }

    // MARK: - Portfolio
    
    /// портфеля цб
    public func portfolio() -> Observable<Payload<Portfolio>> {
        return API.get(url: url("/portfolio" + self.defaultQuery()))
    }
    
    /// валютные активы клиента
    public func portfolioCurrencies() -> Observable<Payload<Currencies>> {
        return API.post(url: url("/portfolio/currencies" + self.defaultQuery()), parameters: [:])
    }
    
    /// данные по инструменту в портфеле
    public func instrumentPortfolio(figi: String? = nil, ticker: String? = nil) -> Observable<PortfolioPosition?> {
        return portfolio().map { (payload) -> PortfolioPosition? in
            for position in payload.payload.positions {
                if let figi = figi, position.figi == figi {
                    return position
                }
                else if let ticker = ticker, position.ticker == ticker {
                    return position
                }
            }
            return nil
        }
    }
    
    // MARK: - Orders
    
    /**
     * Метод для выставления заявки
     * @param figi идентификатор инструмента
     * @param lots количество лотов для заявки
     * @param operation тип заявки
     * @param price цена лимитной заявки
     */
    public func limitOrder(figi: String, order: LimitOrderRequest) -> Observable<Payload<PlacedLimitOrder?>> {
        guard allowOrders else { fatalError("allowOrders: false") }
        let query = ["figi": figi, "brokerAccountId": brokerAccountId ?? ""]
        return API.post(url: url("/orders/limit-order?" + query.toURLString()), parameters: order.dictionary())
    }
    
//    /**
//     * Метод для выставления заявки
//     * @param figi идентификатор инструмента
//     * @param lots количество лотов для заявки
//     * @param operation тип заявки
//     * @param price цена лимитной заявки
//     */
//    marketOrder({ figi, lots, operation }: MarketOrderRequest & FIGI): Promise<PlacedMarketOrder> {
//        return this.makeRequest('/orders/market-order', {
//            method: 'post',
//            query: {
//                figi,
//                brokerAccountId: this.brokerAccountId,
//            },
//            body: {
//                lots,
//                operation,
//            },
//        });
//    }
    
    /// Отмена заявки
    /// - Parameter orderId: ID заявки
    public func cancelOrder(orderId: String) -> Observable<Void> {
        guard allowOrders else { fatalError("allowOrders: false") }
        let query = ["orderId": orderId, "brokerAccountId": brokerAccountId ?? ""]
        return API.postVoid(url: url("/orders/cancel?" + query.toURLString()), parameters: [:])
    }
    
    /// все активные заявки
    public func getOrders() -> Observable<Payload<[Order]>> {
        return API.post(url: url("/orders" + self.defaultQuery()), parameters: [:])
    }
    
    // MARK: - Market
    
    /// доступных валютные инструменты
    public func getCurrencies() -> Observable<Payload<MarketInstrumentList>> { API.get(url: url("/market/currencies")) }
    
    /// доступные валютные ETF
    public func getEtfs() -> Observable<Payload<MarketInstrumentList>> { API.get(url: url("/market/etfs")) }
    
    ///  доступные облигации
    public func getBonds() -> Observable<Payload<MarketInstrumentList>> { API.get(url: url("/market/bonds")) }
    
    /// доступные акции
    public func getStocks() -> Observable<Payload<MarketInstrumentList>> { API.get(url: url("/market/stocks")) }
    
    // MARK: - Graphs
    
    /// Получение исторических свечей по FIGI
    /// - Parameters:
    ///   - from: Начало временного промежутка
    ///   - to: Конец временного промежутка
    ///   - figi: FIGI
    ///   - interval: Интервал свечи. Available values : 1min, 2min, 3min, 5min, 10min, 15min, 30min, hour, day, week, month
    public func candlesGet(fromDate: Date? = nil, from: String? = nil,
                           toDate: Date? = nil, to: String? = nil, figi: String, interval: CandleResolution, encode: Bool = false) -> Observable<Payload<Candles>> {
        guard fromDate != nil || from != nil else { return Observable.error("From parameter is missing")}
        guard toDate != nil || to != nil else { return Observable.error("From parameter is missing")}
        let fromString = (fromDate != nil ? Date.isoFormatterFull.string(from: fromDate!) : from!).replace("+0000", withString: "%2B00:00")
        let toString = (toDate != nil ? Date.isoFormatterFull.string(from: toDate!) : to!).replace("+0000", withString: "%2B00:00")
        let query = ["from": fromString,
                     "to": toString,
                     "figi": figi,
                     "interval": interval.rawValue]
        if encode {
            return API.get(url: url("/market/candles?" + query.toURLString()), parameters: [:])
        }
        return API.get(url: url("/market/candles?from=\(fromString)&to=\(toString)&figi=\(figi)&interval=\(interval.rawValue)"), parameters: [:])
    }
    
//    /**
//     * Метод для получения исторических свечей по FIGI
//     * @param from Начало временного промежутка в формате ISO 8601
//     * @param to Конец временного промежутка в формате ISO 8601
//     * @param figi Figi-идентификатор инструмента
//     * @param interval интервал для свечи
//     */
//    candlesGet({
//        from,
//        to,
//        figi,
//        interval = '1min',
//        }: {
//        from: string;
//        to: string;
//        figi: string;
//        interval?: CandleResolution;
//        }): Promise<Candles> {
//        return this.makeRequest('/market/candles', {
//            query: { from, to, figi, interval },
//        });
//    }
    
    /// Получение стакана по FIGI
    /// - Parameters:
    ///   - figi: Figi-идентификатор инструмента
    ///   - depth: 1-10
    public func getOrderbook(figi: String, depth: Int = 3) -> Observable<Payload<Orderbook>> {
        let query = ["figi": figi, "depth": depth.description]
        return API.get(url: url("/market/orderbook?" + query.toURLString()), parameters: [:])
    }
    
    // MARK: -  Search
    
    /// поиск инструмента по figi или ticker
    /// - Parameters:
    ///   - figi: Figi-идентификатор инструмента
    ///   - ticker: the ticker
    public func search(figi: String?, ticker: String?) -> Observable<Payload<MarketInstrumentList>> {
        guard figi != nil || ticker != nil else { return Observable.error("should specify figi or ticker") }
        var url: String!
        if let figi = figi {
            url = "/market/search/by-figi?figi=\(figi)"
        }
        if let t = ticker {
            url = "/market/search/by-ticker?ticker=\(t)"
        }
        return API.get(url: self.url(url), parameters: [:])
    }

    /// поиск инструмента по figi или ticker
    /// - Parameters:
    ///   - figi: Figi-идентификатор инструмента
    ///   - ticker: the ticker
    public func searchOne(figi: String? = nil, ticker: String? = nil) -> Observable<MarketInstrument?> {
        search(figi: figi, ticker: ticker).map { (list) -> MarketInstrument? in
            return list.payload.instruments.first
        }
    }

    //    /**
    //     * Метод для получения операций по цб по инструменту
    //     * @param from Начало временного промежутка в формате ISO 8601
    //     * @param to Конец временного промежутка в формате ISO 8601
    //     * @param figi Figi-идентификатор инструмента
    //     */
    //    operations({ from, to, figi }: { from: string; to: string; figi?: string }): Promise<Operations> {
    //        return this.makeRequest('/operations', {
    //            query: {
    //                from,
    //                to,
    //                figi,
    //                brokerAccountId: this.brokerAccountId,
    //            },
    //        });
    //    }
    
    
//    /**
//     * Метод для подписки на данные по стакану инструмента
//     * @example
//     * ```typescript
//     * const { figi } = await api.searchOne({ ticker: 'AAPL' });
//     * const unsubFromAAPL = api.orderbook({ figi }, (ob) => { console.log(ob.bids) });
//     * // ... подписка больше не нужна
//     * unsubFromAAPL();
//     * ```
//     * @param figi идентификатор инструмента
//     * @param depth
//     * @param cb функция для обработки новых данных по стакану
//     * @return функция для отмены подписки
//     */
//    orderbook(
//        { figi, depth = 3 }: { figi: string; depth?: Depth },
//        cb: (x: OrderbookStreaming) => any = console.log
//    ) {
//        return this._streaming.orderbook({ figi, depth }, cb);
//    }
//
//    /**
//     * Метод для подписки на данные по свечному графику инструмента
//     * @example см. метод [[orderbook]]
//     * @param figi идентификатор инструмента
//     * @param interval интервал для свечи
//     * @param cb функция для обработки новых данных по свечи
//     * @return функция для отмены подписки
//     */
//    candle(
//        { figi, interval = '1min' }: { figi: string; interval?: Interval },
//        cb: (x: CandleStreaming, metaParams: CandleStreamingMetaParams) => any = console.log
//    ) {
//        return this._streaming.candle({ figi, interval }, cb);
//    }
//
//    /**
//     * Метод для подписки на данные по инструменту
//     * @example см. метод [[orderbook]]
//     * @param figi идентификатор инструмента
//     * @param cb функция для обработки новых данных по инструменту
//     * @return функция для отмены подписки
//     */
//    instrumentInfo({ figi }: { figi: string }, cb: (x: InstrumentInfoStreaming, metaParams: InstrumentInfoStreamingMetaParams) => any = console.log) {
//        return this._streaming.instrumentInfo({ figi }, cb);
//    }
//
//
//    /**
//     * Метод для обработки сообщений об ошибки от стриминга
//     * @example
//     * ```typescript
//     * api.onStreamingError(({ error }) => { console.log(error) });
//     * api.instrumentInfo({ figi: 'NOOOOOOO' }, (ob) => { console.log(ob.bids) });
//     * // logs:  Subscription instrument_info:subscribe. FIGI NOOOOOOO not found
//     * ```
//     * @param cb функция для обработки всех ошибок от стриминга
//     * @return функция для отмены подписки
//     */
//    onStreamingError(cb: (x: StreamingError, metaParams: InstrumentInfoStreamingMetaParams) => any) {
//        return this._streaming.onStreamingError(cb);
//    }
//
//    /**
//     * Метод для получения брокерских счетов клиента
//     */
//    accounts(): Promise<UserAccounts> {
//        return this.makeRequest('/user/accounts');
//    }
    // MARK: - Private
    
    private func url(_ endpoint: String) -> String {
        return apiURL + endpoint
    }
    
    private func defaultQuery() -> String {
        let query = ["brokerAccountId":
                        brokerAccountId ?? ""]
        return "?" + query.toURLString()
    }
}

extension RestServiceApi {
    
}

extension Date {

    /// ISO date formatter
    public static let isoFormatterFull: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        df.timeZone = TimeZone(secondsFromGMT: 0)
        return df
    }()
}

extension RestServiceApi {
    
    /// POST request
    ///
    /// - Parameter url: URL
    /// - Returns: the observable
    public static func postVoid(url: URLConvertible, parameters: [String: Any]) -> Observable<Void> {
        return requestString(.post, url: url, parameters: parameters)
            .map { (_) -> Void in
                return
            }
    }
    
    /// Request to API with URLEncoding encoding
    ///
    /// - Parameters:
    ///   - method: the method
    ///   - url: the URL
    ///   - parameters: the parameters
    ///   - headers: the headers
    ///   - encoding: the encoding
    /// - Returns: the observable
    public static func requestString(_ method: HTTPMethod,
                                     url: URLConvertible,
                                     parameters: [String: Any]? = nil,
                                     headers: [String: String] = [:],
                                     encoding: ParameterEncoding = URLEncoding.default) -> Observable<Data> {
        var headers = headers
        for (k,v) in RestServiceApi.headers {
            headers[k] = v
        }
        var sendRequest: URLRequest!
        return RxAlamofire
            .request(method, url, parameters: parameters, encoding: encoding, headers: headers)
            .observeOn(ConcurrentDispatchQueueScheduler.init(qos: .default))
            .do(onNext: { (request) in
                if let request = request.request {
                    sendRequest = request
                    logRequest(request)
                }
            })
            .responseData()
            .flatMap { (response: HTTPURLResponse, data: Data) -> Observable<Data> in
                logResponse(data as AnyObject, forRequest: sendRequest, response: response)
                
                if UNAUTHORIZED_CODES.contains(response.statusCode) {
                    if let callback401 = callback401 { DispatchQueue.main.async { callback401() } }
                    return Observable.error(NSLocalizedString("Unauthorized", comment: "Unauthorized"))
                }
                else if response.statusCode >= 400 {
                    let error: Error? = JSON(data)["message"].stringValue
                    checkResponseForUnauthorizedErrors(data: data)
                    return Observable.error(error as? String ?? NSLocalizedString("Unknown error", comment: "Unknown error"))
                }
                return Observable.just(data)
            }
    }
}
