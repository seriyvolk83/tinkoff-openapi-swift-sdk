//
//  GeneralTypes.swift
//  TinkoffSample
//
//  Created by Volkov Alexander on 6/11/21.
//

import Foundation

//public struct Interval {
//    public static let m1 = "1min"
//    public static let m2 = "2min"
//    public static let m3 = "3min"
//    public static let m5 = "5min"
//    public static let m10 = "10min"
//    public static let m15 = "15min"
//    public static let m30 = "30min"
//    public static let h1 = "hour"
//    public static let h2 = "2hour"
//    public static let h4 = "4hour"
//    public static let d = "day"
//    public static let w = "week"
//    public static let m = "month"
//}
//public struct Depth = 1api 2api 3api 4api 5api 6api 7api 8api 9api 10;
//public struct HttpMethod = 'get" 'post';
//public struct SocketEventType = 'orderbook" 'candle" 'instrument_info';
//public struct Dict<T>: Codable { [x: string]: T }
//public struct OrderbookStreaming: Codable {
//    public let figi: String
//    public let depth: Depth;
//    public let bids: Array<[number, number]>;
//    public let asks: Array<[number, number]>;
//}
//public struct OrderbookStreamingMetaParams: Codable {
//    public let /** Серверное время в формате RFC3339Nano */
//    public let serverTime: String
//}
// Not needed
//public struct InstrumentId: Codable {
//    public let ticker: String?
//    public let figi: String?
//}
//public struct CandleStreaming: Codable {
//    public let o: Double
//    public let c: Double
//    public let h: Double
//    public let l: Double
//    public let v: Double
//    public let time: String
//    public let interval: Interval;
//    public let figi: String
//}
//public struct CandleStreamingMetaParams: Codable {
//    public let /** Серверное время в формате RFC3339Nano */
//    public let serverTime: String
//}
//public struct InstrumentInfoStreaming: Codable {
//    public let /** Статус торгов */
//    public let trade_status: String
//    public let /** Шаг цены */
//    public let min_price_increment: Double
//    public let /** Лот */
//    public let lot: Double
//    public let /** НКД. Возвращается только для бондов */
//    public let accrued_interest?: Double
//    public let /** Верхняя граница заявки. Возвращается только для RTS инструментов */
//    public let limit_up?: Double
//    public let /** Нижняя граница заявки. Возвращается только для RTS инструментов */
//    public let limit_down?: Double
//    public let /** FIGI */
//    public let figi: String
//}
public struct StreamingError: Codable {
    public let request_id: String
    public let error: String
}
public struct InstrumentInfoStreamingMetaParams: Codable {
    /** Серверное время в формате RFC3339Nano */
    public let serverTime: String
}

public struct LimitOrderParams: Codable {
    public let figi: String
    public let lots: Double
    public let operation: OperationType
    public let price: Double
}

public struct FIGI: Codable {
    public let figi: String
}
