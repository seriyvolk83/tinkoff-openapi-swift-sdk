//
//  Models.swift
//  TinkoffSample
//
//  Created by Volkov Alexander on 6/11/21.
//

import Foundation

public struct Payload<T: Codable>: Codable {
    public let trackingId: String
    public let payload: T
    public let status: String
    
    public init(trackingId: String, payload: T, status: String) {
        self.trackingId = trackingId
        self.payload = payload
        self.status = status
    }
}

//public struct Empty {
//    public let trackingId: String
//    public let payload: Any
//    public let status: String
//}
//
//public struct Error: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: ErrorPayload
//}
//
//public struct PortfolioResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: Portfolio;
//}

public struct Portfolio: Codable {
    public let positions: [PortfolioPosition]
}

public struct UserAccountsResponse: Codable {
    public let trackingId: String
    public let status: String
    public let payload: UserAccounts;
}

public struct UserAccounts: Codable {
    public let accounts: [UserAccount]
}

public struct UserAccount: Codable {
    public let brokerAccountType: BrokerAccountType
    public let brokerAccountId: String
}

//public struct PortfolioCurrenciesResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: Currencies
//}

public struct Currencies: Codable {
    public let currencies: [CurrencyPosition]
}

public struct CurrencyPosition: Codable {
    public let currency: Currency;
    public let balance: Double
    public let blocked: Double?
}

public struct PortfolioPosition: Codable {
    public let figi: String
    public let ticker: String?
    public let isin: String?
    public let instrumentType: InstrumentType;
    public let balance: Double
    public let blocked: Double?
    public let expectedYield: MoneyAmount?
    public let lots: Double
    public let averagePositionPrice: MoneyAmount?
    public let averagePositionPriceNoNkd: MoneyAmount?
    public let name: String
}

public struct MoneyAmount: Codable {
    public let currency: Currency;
    public let value: Double
}

public struct Orderbook: Codable {
    public let figi: String
    public let depth: Double
    public let bids: [OrderResponse]
    public let asks: [OrderResponse]
    public let tradeStatus: TradeStatus;
    public let minPriceIncrement: Double
    public let faceValue: Double?
    public let lastPrice: Double?
    public let closePrice: Double?
    public let limitUp: Double?
    public let limitDown: Double?
}

public struct OrderResponse: Codable {
    public let price: Double
    public let quantity: Double
}

public struct Candles: Codable {
    public let figi: String
    public let interval: CandleResolution
    public let candles: [Candle]
}

public struct Candle: Codable {
    public let figi: String
    public let interval: CandleResolution
    public let o: Double
    public let c: Double
    public let h: Double
    public let l: Double
    public let v: Double
    public let time: String
}

//public struct OperationsResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: Operations;
//}
//
//public struct Operations: Codable {
//    public let operations: [Operation]
//}
//
//public struct OperationTrade: Codable {
//    public let tradeId: String
//    public let date: String
//    public let price: Double
//    public let quantity: Double
//}
//
//public struct Operation: Codable {
//    public let id: String
//    public let status: OperationStatus
//    public let trades: [OperationTrade]?
//    public let commission: MoneyAmount?
//    public let currency: Currency
//    public let payment: Double
//    public let price: Double?
//    public let quantity: Double?
//    public let quantityExecuted: Double?
//    public let figi: String?
//    public let instrumentType: InstrumentType?
//    public let isMarginCall: Bool
//    public let date: String
//    public let operationType: OperationTypeWithCommission?
//}

public struct Order: Codable {
    public let orderId: String
    public let figi: String
    public let operation: OperationType;
    public let status: OrderStatus
    public let requestedLots: Double
    public let executedLots: Double
    public let type: OrderType;
    public let price: Double
}

public struct LimitOrderRequest: Codable {
    public let lots: Double
    public let operation: OperationType;
    public let price: Double
    
    public init(lots: Double, operation: OperationType, price: Double) {
        self.lots = lots
        self.operation = operation
        self.price = price
    }
}

public struct PlacedLimitOrder: Codable {
    public let orderId: String
    public let operation: OperationType;
    public let status: OrderStatus
    public let rejectReason: String?
    public let message: String?
    public let requestedLots: Double
    public let executedLots: Double
    public let commission: MoneyAmount?
}

//public struct MarketOrderRequest: Codable {
//    public let lots: Double
//    public let operation: OperationType;
//}
//
//public struct MarketOrderResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: PlacedMarketOrder;
//}
//
//public struct PlacedMarketOrder: Codable {
//    public let orderId: String
//    public let operation: OperationType
//    public let status: OrderStatus
//    public let rejectReason: String?
//    public let message: String?
//    public let requestedLots: Double
//    public let executedLots: Double
//    public let commission: MoneyAmount?
//}
//
//
//
//public struct SandboxRegisterRequest: Codable {
//    public let brokerAccountType: BrokerAccountType?
//}
//
//public struct SandboxRegisterResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: SandboxAccount;
//}
//
//public struct SandboxAccount: Codable {
//    public let brokerAccountType: BrokerAccountType
//    public let brokerAccountId: String
//}

public struct SandboxSetCurrencyBalanceRequest: Codable {
    public let currency: SandboxCurrency
    public let balance: Double
    
    public init(currency: SandboxCurrency, balance: Double) {
        self.currency = currency
        self.balance = balance
    }
}

public struct SandboxSetPositionBalanceRequest: Codable {
    public let figi: String?
    public let balance: Double
    
    public init(figi: String?, balance: Double) {
        self.figi = figi
        self.balance = balance
    }
}

public struct MarketInstrumentList: Codable {
    public let total: Double
    public let instruments: [MarketInstrument]
}

//public struct SearchMarketInstrumentResponse: Codable {
//    public let trackingId: String
//    public let status: String
//    public let payload: SearchMarketInstrument;
//}

//public struct SearchMarketInstrument: Codable {
//    public let figi: String
//    public let ticker: String
//    public let isin: String?
//    public let minPriceIncrement: Double?
//    public let lot: Double
//    public let currency: Currency?
//    public let name: String
//    public let type: InstrumentType;
//}

public struct MarketInstrument: Codable {
    public let figi: String
    public let ticker: String
    public let isin: String?
    public let minPriceIncrement: Double?
    public let lot: Double
    public let minQuantity: Double?
    public let currency: Currency?
    public let name: String
    public let type: InstrumentType
}

//public struct ErrorPayload: Codable {
//    public let message: String?
//    public let code: String?
//}
